module gitee.com/chunanyong/zorm

go 1.13

require (
	gitee.com/chunanyong/gouuid v1.3.2
	github.com/shopspring/decimal v1.2.0
)
